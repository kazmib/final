class Student:
    
    def __init__(self, id, name,midterm, final):
        self.id = id
        self.name = name
        self.midterm = midterm
        self.final = final
    
    def totalMarks(self):
        try:
            self.totalmarks =  self.midterm + self.final
        except:
            print("wrong data types, please input correct format")
        
        return self.totalmarks
        
    def status(self):
        totalmarks = self.totalMarks()
        if self.totalmarks > 50:
            return "Pass"
        else:
            return "Fail"
           

def Create_Students():    
    students = []
    f = open("Downloads/input_file.csv", "r")
    lines = f.readlines()[1:]

    for line in lines:
        line = line.split(',')
        student = Student(line[0], line[1], float(line[2]), float(line[3]))
        students.append(student)

    f.close()

    f = open("output.csv", "w")
    allStudents = []
    for student in students:
        status = []

        status.append(student.id)
        status.append(student.name)
        status.append(str(student.totalMarks()))
        status.append(student.status())
        allStudents.append(status)
        output = ','.join(status)
        f.write(output + '\n')    

    f.close()

    print("Number of students:" + str(len(allStudents)))

    totalMark = 0
    for i in range(len(allStudents)):
        totalMark += float(allStudents[i][2])

    average = totalMark / len(allStudents)

    print("Average Grade: " + str(average))

    maxMark = 0
    maxName = ''
    for i in range(len(allStudents)):
        if float(allStudents[i][2]) > maxMark:
            maxMark = float(allStudents[i][2]) 
            maxName = allStudents[i][1]


    print("Highest mark is " + str(maxMark) + " and belong to " + maxName )

Create_Students()